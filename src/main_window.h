#ifndef SVP_MAINWINDOW_H
#define SVP_MAINWINDOW_H

#include <QMainWindow>

namespace SVP {

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	Ui::MainWindow *ui;
};

} // namespace SVP
#endif // SVP_MAINWINDOW_H
